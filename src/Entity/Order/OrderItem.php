<?php

declare(strict_types=1);

namespace App\Entity\Order;

use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\OrderItem as BaseOrderItem;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_order_item")
 */
class OrderItem extends BaseOrderItem
{
    /**
     * @var OrderItem|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Order\OrderItem")
     * @ORM\JoinColumn(name="parent_item_id", referencedColumnName="id")
     */
    private $parentItem;

    public function getParentItem(): ?OrderItem
    {
        return $this->parentItem;
    }

    public function setParentItem(OrderItem $parentItem): void
    {
        $this->parentItem = $parentItem;
    }
}
