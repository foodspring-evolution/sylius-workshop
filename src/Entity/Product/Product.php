<?php

declare(strict_types=1);

namespace App\Entity\Product;

use App\Entity\Supplier;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Product as BaseProduct;
use Sylius\Component\Product\Model\ProductTranslationInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product")
 */
class Product extends BaseProduct
{
    private const TYPE_REGULAR = 'regular';
    private const TYPE_BUNDLE = 'bundle';

    /**
     * @var Supplier|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Supplier", inversedBy="products")
     * @ORM\JoinColumn(name="supplier_id", referencedColumnName="id")
     */
    private $supplier;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $type = self::TYPE_REGULAR;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Product\Product", mappedBy="bundleParents")
     */
    private $bundleProducts;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Product\Product", inversedBy="bundleProducts")
     * @ORM\JoinTable(name="app_product_bundle_parents",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="bundle_parent_id", referencedColumnName="id")}
     *      )
     * )
     */
    private $bundleParents;

    public function __construct()
    {
        parent::__construct();

        $this->bundleProducts = new ArrayCollection();
        $this->bundleParents = new ArrayCollection();
    }

    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(?Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function makeBundle(): void
    {
        $this->type = self::TYPE_BUNDLE;
    }

    public function isBundle(): bool
    {
        return $this->type === self::TYPE_BUNDLE;
    }

    protected function createTranslation(): ProductTranslationInterface
    {
        return new ProductTranslation();
    }

    public function addBundleProduct(Product $product): void
    {
        $product->addBundleParent($this);
        $this->bundleProducts->add($product);
    }

    public function addBundleParent(Product $product): void
    {
        $this->bundleParents->add($product);
    }

    public function removeBundleParent(Product $product): void
    {
        $this->bundleParents->removeElement($product);
    }

    public function getBundleProducts(): Collection
    {
        return $this->bundleProducts;
    }

    public function removeBundleProduct(Product $product): void
    {
        $product->removeBundleParent($this);
        $this->bundleProducts->removeElement($product);
    }
}
