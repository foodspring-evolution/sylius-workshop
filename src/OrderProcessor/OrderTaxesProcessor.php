<?php

declare(strict_types=1);

namespace App\OrderProcessor;

use App\Entity\Addressing\Address;
use Sylius\Component\Order\Model\OrderInterface;
use Sylius\Component\Order\Processor\OrderProcessorInterface;

final class OrderTaxesProcessor implements OrderProcessorInterface
{
    /** @var OrderProcessorInterface */
    private $defaultOrderTaxesProcessor;

    public function __construct(OrderProcessorInterface $defaultOrderTaxesProcessor)
    {
        $this->defaultOrderTaxesProcessor = $defaultOrderTaxesProcessor;
    }

    public function process(OrderInterface $order): void
    {
        if (!$this->taxesShouldBeApplied($order)) {
            return;
        }

        $this->defaultOrderTaxesProcessor->process($order);
    }

    private function taxesShouldBeApplied(OrderInterface $order): bool
    {
        /** @var Address $billingAddress */
        $billingAddress = $order->getBillingAddress();

        if ($billingAddress === null) {
            return true;
        }

        return $billingAddress->getVatNumber() === null;
    }
}
