<?php

declare(strict_types=1);

namespace App\Shipping\Calculator;

use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\OrderItemInterface;
use Sylius\Component\Shipping\Calculator\CalculatorInterface;
use Sylius\Component\Shipping\Model\ShipmentInterface;

final class WeightBasedShippingCalculator implements CalculatorInterface
{
    public function calculate(ShipmentInterface $subject, array $configuration): int
    {
        /** @var OrderInterface $order */
        $order = $subject->getOrder();
        $weight = $this->weighOrder($order);

        if ($weight >= $configuration['limit_weight']) {
            return $configuration['above_or_equal'];
        }

        return $configuration['below'];
    }

    public function getType(): string
    {
        return 'weight_based';
    }

    private function weighOrder(OrderInterface $order): float
    {
        $totalWeight = 0.0;

        /** @var OrderItemInterface $item */
        foreach ($order->getItems() as $item) {
            $totalWeight += $item->getVariant()->getWeight() * $item->getQuantity();
        }

        return $totalWeight;
    }
}
