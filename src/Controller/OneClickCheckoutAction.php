<?php

declare(strict_types=1);

namespace App\Controller;

use Doctrine\Persistence\ObjectManager;
use SM\Factory\FactoryInterface as StateMachineFactoryInterface;
use Sylius\Component\Core\Context\ShopperContextInterface;
use Sylius\Component\Core\Model\CustomerInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\OrderItemInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Sylius\Component\Core\OrderCheckoutTransitions;
use Sylius\Component\Core\Repository\ProductVariantRepositoryInterface;
use Sylius\Component\Order\Modifier\OrderItemQuantityModifierInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

final class OneClickCheckoutAction
{
    /** @var ProductVariantRepositoryInterface */
    private $productVariantRepository;

    /** @var FactoryInterface */
    private $orderFactory;

    /** @var FactoryInterface */
    private $orderItemFactory;

    /** @var OrderItemQuantityModifierInterface */
    private $orderItemQuantityModifier;

    /** @var ShopperContextInterface */
    private $shopperContext;

    /** @var StateMachineFactoryInterface */
    private $stateMachineFactory;

    /** @var ObjectManager */
    private $orderManager;

    public function __construct(
        ProductVariantRepositoryInterface $productVariantRepository,
        FactoryInterface $orderFactory,
        FactoryInterface $orderItemFactory,
        OrderItemQuantityModifierInterface $orderItemQuantityModifier,
        ShopperContextInterface $shopperContext,
        StateMachineFactoryInterface $stateMachineFactory,
        ObjectManager $orderManager
    ) {
        $this->productVariantRepository = $productVariantRepository;
        $this->orderFactory = $orderFactory;
        $this->orderItemFactory = $orderItemFactory;
        $this->orderItemQuantityModifier = $orderItemQuantityModifier;
        $this->shopperContext = $shopperContext;
        $this->stateMachineFactory = $stateMachineFactory;
        $this->orderManager = $orderManager;
    }

    public function __invoke(Request $request): Response
    {
        $order = $this->prepareOrder($request);
        $this->setOrderData($order);

        $stateMachine = $this->stateMachineFactory->get($order, OrderCheckoutTransitions::GRAPH);
        $stateMachine->apply(OrderCheckoutTransitions::TRANSITION_ADDRESS);
        $stateMachine->apply(OrderCheckoutTransitions::TRANSITION_SELECT_SHIPPING);
        $stateMachine->apply(OrderCheckoutTransitions::TRANSITION_SELECT_PAYMENT);
        $stateMachine->apply(OrderCheckoutTransitions::TRANSITION_COMPLETE);

        $this->orderManager->persist($order);
        $this->orderManager->flush();

        $session = $request->getSession();
        /** @var FlashBagInterface $flashBag */
        $flashBag = $session->getBag('flashes');
        $flashBag->add('success', 'Thank you for ordering!');

        return new RedirectResponse($request->headers->get('referer'));
    }

    private function prepareOrder(Request $request): OrderInterface
    {
        $variantId = $request->attributes->get('variantId');
        /** @var ProductVariantInterface $variant */
        $variant = $this->productVariantRepository->find($variantId);

        /** @var OrderInterface $order */
        $order = $this->orderFactory->createNew();
        /** @var OrderItemInterface $orderItem */
        $orderItem = $this->orderItemFactory->createNew();
        $orderItem->setVariant($variant);

        $this->orderItemQuantityModifier->modify($orderItem, 1);
        $order->addItem($orderItem);

        return $order;
    }

    private function setOrderData(OrderInterface $order): void
    {
        /** @var CustomerInterface $customer */
        $customer = $this->shopperContext->getCustomer();
        $order->setCustomer($customer);
        $order->setShippingAddress(clone $customer->getDefaultAddress());
        $order->setBillingAddress(clone $customer->getDefaultAddress());

        $order->setChannel($this->shopperContext->getChannel());
        $order->setCurrencyCode($this->shopperContext->getCurrencyCode());
        $order->setLocaleCode($this->shopperContext->getLocaleCode());
    }
}
