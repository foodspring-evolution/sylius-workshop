<?php

declare(strict_types=1);

namespace App\Payum\Action;

use App\Payum\Model\TestApi;
use GuzzleHttp\Client;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\Exception\UnsupportedApiException;
use Payum\Core\Request\Capture;
use Sylius\Component\Core\Model\PaymentInterface;

final class CaptureAction implements ActionInterface, ApiAwareInterface
{
    /** @var TestApi */
    private $api;

    /** @var Client */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param Capture $request
     */
    public function execute($request)
    {
        /** @var PaymentInterface $payment */
        $payment = $request->getModel();

//        $response = $this->client->request('POST', 'URL', ['body' => ['id' => $payment->getOrder()->getId()]]);

        $payment->setDetails(['status' => 200]);
    }

    public function supports($request)
    {
        return
            $request instanceof Capture &&
            $request->getFirstModel() instanceof PaymentInterface
        ;
    }

    public function setApi($api)
    {
        if (!$api instanceof TestApi) {
            throw new UnsupportedApiException();
        }

        $this->api = $api;
    }
}
