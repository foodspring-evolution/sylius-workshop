<?php

declare(strict_types=1);

namespace App\Payum\Factory;

use App\Payum\Action\StatusAction;
use App\Payum\Model\TestApi;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;

final class TestPaymentGatewayFactory extends GatewayFactory
{
    protected function populateConfig(ArrayObject $config)
    {
        $config->defaults([
            'payum.factory_name' => 'test',
            'payum.factory_title' => 'Test',
            'payum.action.status' => new StatusAction(),
        ]);

        $config['payum.api'] = function (ArrayObject $config) {
            return new TestApi($config['api_key']);
        };
    }
}
