<?php

declare(strict_types=1);

namespace App\Modifier;

use App\Entity\Order\OrderItem;
use App\Entity\Product\Product;
use Sylius\Component\Order\Model\OrderInterface;
use Sylius\Component\Order\Model\OrderItemInterface;
use Sylius\Component\Order\Modifier\OrderItemQuantityModifierInterface;
use Sylius\Component\Order\Modifier\OrderModifierInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;

final class OrderModifier implements OrderModifierInterface
{
    /** @var OrderModifierInterface */
    private $baseOrderModifier;

    /** @var FactoryInterface */
    protected $orderItemFactory;

    /** @var OrderItemQuantityModifierInterface */
    private $orderItemQuantityModifier;

    public function __construct(
        OrderModifierInterface $baseOrderModifier,
        FactoryInterface $orderItemFactory,
        OrderItemQuantityModifierInterface $orderItemQuantityModifier
    ) {
        $this->baseOrderModifier = $baseOrderModifier;
        $this->orderItemFactory = $orderItemFactory;
        $this->orderItemQuantityModifier = $orderItemQuantityModifier;
    }

    public function addToOrder(OrderInterface $cart, OrderItemInterface $cartItem): void
    {
        /** @var Product $product */
        $product = $cartItem->getProduct();
        if (!$product->isBundle()) {
            $this->baseOrderModifier->addToOrder($cart, $cartItem);

            return;
        }

        /** @var Product $bundleProduct */
        foreach ($product->getBundleProducts() as $bundleProduct) {
            /** @var OrderItem $orderItem */
            $orderItem = $this->orderItemFactory->createNew();
            $orderItem->setVariant($bundleProduct->getVariants()->first());
            $orderItem->setParentItem($cartItem);
            $this->orderItemQuantityModifier->modify($orderItem, 1);

            $cart->addItem($orderItem);
        }

        $this->baseOrderModifier->addToOrder($cart, $cartItem);
    }

    public function removeFromOrder(OrderInterface $cart, OrderItemInterface $item): void
    {
        $this->baseOrderModifier->removeFromOrder($cart, $item);
    }
}
