<?php

declare(strict_types=1);

namespace App\Fixture;

use App\Entity\Supplier;
use Doctrine\Persistence\ObjectManager;
use Faker\Generator;
use Sylius\Bundle\FixturesBundle\Fixture\AbstractFixture;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

final class SupplierFixture extends AbstractFixture
{
    /** @var FactoryInterface */
    private $supplierFactory;

    /** @var ObjectManager */
    private $supplierManager;

    /** @var Generator */
    private $faker;

    public function __construct(FactoryInterface $supplierFactory, ObjectManager $supplierManager, Generator $faker)
    {
        $this->supplierFactory = $supplierFactory;
        $this->supplierManager = $supplierManager;
        $this->faker = $faker;
    }

    public function load(array $options): void
    {
        for ($i = 0; $i < $options['count']; $i++) {
            /** @var Supplier $supplier */
            $supplier = $this->supplierFactory->createNew();
            $supplier->setName($this->faker->company);
            $supplier->setEmail($this->faker->companyEmail);

            $this->supplierManager->persist($supplier);
        }

        $this->supplierManager->flush();
    }

    public function getName(): string
    {
        return 'supplier';
    }

    protected function configureOptionsNode(ArrayNodeDefinition $optionsNode): void
    {
        $optionsNode
            ->children()
            ->integerNode('count')
        ;
    }
}
