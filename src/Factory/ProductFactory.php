<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Product\Product;
use Sylius\Component\Product\Factory\ProductFactory as BaseProductFactory;
use Sylius\Component\Product\Factory\ProductFactoryInterface as BaseProductFactoryInterface;
use Sylius\Component\Product\Model\ProductInterface;

final class ProductFactory implements ProductFactoryInterface
{
    /** @var BaseProductFactoryInterface */
    private $decoratedProductFactory;

    public function __construct(BaseProductFactoryInterface $decoratedProductFactory)
    {
        $this->decoratedProductFactory = $decoratedProductFactory;
    }

    public function createBundle(): Product
    {
        /** @var Product $product */
        $product = $this->decoratedProductFactory->createNew();
        $product->makeBundle();

        return $product;
    }

    public function createNew()
    {
        return $this->decoratedProductFactory->createNew();
    }

    public function createWithVariant(): ProductInterface
    {
        return $this->decoratedProductFactory->createWithVariant();
    }
}
