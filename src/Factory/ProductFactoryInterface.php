<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Product\Product;
use Sylius\Component\Product\Factory\ProductFactoryInterface as BaseProductFactoryInterface;

interface ProductFactoryInterface extends BaseProductFactoryInterface
{
    public function createBundle(): Product;
}
