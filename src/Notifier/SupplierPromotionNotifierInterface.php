<?php

declare(strict_types=1);

namespace App\Notifier;

use App\Entity\Supplier;

interface SupplierPromotionNotifierInterface
{
    public function notify(Supplier $supplier): void;
}
