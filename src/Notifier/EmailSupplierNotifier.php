<?php

declare(strict_types=1);

namespace App\Notifier;

use App\Entity\Supplier;
use Sylius\Component\Mailer\Sender\SenderInterface;

final class EmailSupplierNotifier implements SupplierPromotionNotifierInterface
{
    /** @var SenderInterface */
    private $sender;

    public function __construct(SenderInterface $sender)
    {
        $this->sender = $sender;
    }

    public function notify(Supplier $supplier): void
    {
        $this->sender->send('app_supplier_promoted', [$supplier->getEmail()], ['supplier' => $supplier]);
    }
}
