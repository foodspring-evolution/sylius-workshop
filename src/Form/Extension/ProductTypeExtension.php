<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Entity\Supplier;
use Sylius\Bundle\ProductBundle\Form\Type\ProductAutocompleteChoiceType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;

final class ProductTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('supplier', EntityType::class, [
                'class' => Supplier::class,
                'placeholder' => 'Select a supplier',
                'choice_label' => 'name',
            ])
            ->add('bundleProducts', ProductAutocompleteChoiceType::class, [
                'label' => 'Bundle products',
                'multiple' => true,
            ])
        ;
    }

    public static function getExtendedTypes(): iterable
    {
        yield ProductType::class;
    }
}
