<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Entity\Order\Order;
use App\Form\Type\TestPaymentType;
use Sylius\Bundle\CoreBundle\Form\Type\Checkout\CompleteType;
use Sylius\Component\Core\Model\PaymentInterface;
use Sylius\Component\Core\Model\PaymentMethodInterface;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

final class CompleteTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            /** @var Order $order */
            $order = $event->getData();
            $form = $event->getForm();

            /** @var PaymentInterface $payment */
            $payment = $order->getLastPayment(PaymentInterface::STATE_CART);
            /** @var PaymentMethodInterface $paymentMethod */
            $paymentMethod = $payment->getMethod();

            if ($paymentMethod->getGatewayConfig()->getGatewayName() === 'test') {
                $form->add('details', TestPaymentType::class, [
                    'property_path' => 'payments[0].details',
                    'label' => false,
                ]);
            }
        });
    }

    public function getExtendedTypes(): iterable
    {
        yield CompleteType::class;
    }
}
