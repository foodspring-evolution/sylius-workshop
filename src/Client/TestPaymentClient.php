<?php

declare(strict_types=1);

namespace App\Client;

use GuzzleHttp\Client;

final class TestPaymentClient implements TestPaymentClientInterface
{
    /** @var Client */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function validate(string $token): bool
    {
        $response = $this->client->request('POST', 'https://sylius-test-payment.free.beeceptor.com/pay', [
            'form_params' => [
                'token' => $token
            ]
        ]);

        $content = json_decode($response->getBody()->getContents(), true);

        return $content['valid'];
    }
}
