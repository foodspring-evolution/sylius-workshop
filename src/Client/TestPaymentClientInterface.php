<?php

declare(strict_types=1);

namespace App\Client;

interface TestPaymentClientInterface
{
    public function validate(string $token): bool;
}
