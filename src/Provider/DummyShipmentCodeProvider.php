<?php

declare(strict_types=1);

namespace App\Provider;

use Sylius\Component\Core\Model\ShipmentInterface;

final class DummyShipmentCodeProvider implements ShipmentCodeProviderInterface
{
    public function provide(ShipmentInterface $shipment): string
    {
        return (string) rand(1000, 100000);
    }
}
