<?php

declare(strict_types=1);

namespace App\Menu;

use App\Entity\Product\Product;
use Sylius\Bundle\AdminBundle\Event\ProductMenuBuilderEvent;

final class ProductFormMenuListener
{
    public function addBundleTab(ProductMenuBuilderEvent $event): void
    {
        /** @var Product $product */
        $product = $event->getProduct();
        if (!$product->isBundle()) {
            return;
        }

        $event
            ->getMenu()
                ->addChild('bundle')
                ->setAttribute('template', 'ProductBundle/bundleProducts.html.twig')
                ->setLabel('Bundle')
        ;
    }
}
