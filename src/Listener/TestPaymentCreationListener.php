<?php

declare(strict_types=1);

namespace App\Listener;

use App\Client\TestPaymentClientInterface;
use App\Entity\Order\Order;
use SM\Factory\FactoryInterface;
use Sylius\Component\Core\Model\PaymentInterface;
use Sylius\Component\Core\Model\PaymentMethodInterface;
use Sylius\Component\Payment\PaymentTransitions;
use Symfony\Component\EventDispatcher\GenericEvent;

final class TestPaymentCreationListener
{
    /** @var TestPaymentClientInterface */
    private $testPaymentClient;

    /** @var FactoryInterface */
    private $stateMachineFactory;

    public function __construct(TestPaymentClientInterface $testPaymentClient, FactoryInterface $stateMachineFactory)
    {
        $this->testPaymentClient = $testPaymentClient;
        $this->stateMachineFactory = $stateMachineFactory;
    }

    public function validateTestPayment(GenericEvent $event): void
    {
        /** @var Order $order */
        $order = $event->getSubject();

        $payment = $order->getLastPayment(PaymentInterface::STATE_CART);
        /** @var PaymentMethodInterface $paymentMethod */
        $paymentMethod = $payment->getMethod();

        if ($paymentMethod->getGatewayConfig()->getGatewayName() !== 'test') {
            return;
        }

        $details = $payment->getDetails();

        if (!$this->testPaymentClient->validate($details['token'])) {
            throw new \Exception('Wrong token!');
        }

        $stateMachine = $this->stateMachineFactory->get($payment, PaymentTransitions::GRAPH);
        $stateMachine->apply(PaymentTransitions::TRANSITION_CREATE);
        $stateMachine->apply(PaymentTransitions::TRANSITION_COMPLETE);
    }
}
