<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200710115021 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE app_product_bundle_parents (product_id INT NOT NULL, bundle_parent_id INT NOT NULL, INDEX IDX_75DB69A54584665A (product_id), INDEX IDX_75DB69A53E3AF51F (bundle_parent_id), PRIMARY KEY(product_id, bundle_parent_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_product_bundle_parents ADD CONSTRAINT FK_75DB69A54584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id)');
        $this->addSql('ALTER TABLE app_product_bundle_parents ADD CONSTRAINT FK_75DB69A53E3AF51F FOREIGN KEY (bundle_parent_id) REFERENCES sylius_product (id)');
        $this->addSql('ALTER TABLE sylius_product ADD type VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE app_product_bundle_parents');
        $this->addSql('ALTER TABLE sylius_product DROP type');
    }
}
